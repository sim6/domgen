#!/bin/sh


CHARS=$2
[ -z "$CHARS" ] && CHARS=5

TLDS="$(cat conf/tlds)"

DATA=data
SLEEP=3

check()
{
	if [ -n "$1" ]
	then
		name="$1"
		echo -n "* $name"
		mkdir -p $DATA/$name
		for tld in $TLDS
		do
			resp=""
			echo -n " $tld"
			while ! whois="$(whois $name.$tld)"
			do
				sleep $(expr $SLEEP \* 4)
			done
			if echo "$whois" \
				| grep -q "\(No match for\|NOT FOUND\)"
			then
				mkdir -p $DATA/$name/available \
					&& touch $DATA/$name/available/$tld \
					&& resp="OK"
				rm -f $DATA/$name/unavailable/$tld
			else
				if echo "$whois" \
					| grep -q "\(Registrar:\|Domain ID:\)"
				then
					mkdir -p $DATA/$name/unavailable \
						&& touch $DATA/$name/unavailable/$tld \
						&& resp="KO"
					rm -f $DATA/$name/available/$tld
				fi
			fi

			if [ -z "$resp" ]
			then
				echo ERROR
				echo "$whois"
			else
				echo -n " $resp"
			fi
			sleep $SLEEP
		done
		echo ""
	fi
}

if [ "$1" = "add" ]
then
	check $2
	exit
fi

if [ -z $1 ]
then
	for name in $(ls $DATA)
	do
		check $name
		sleep $(expr $SLEEP \* 2)
	done
	exit
fi

count=0

while [ -z "$1" ] || [ $count -lt "$1" ]
do
	name=$(pwgen -0 -A $CHARS 1)
	check $name
	sleep $SLEEP
	count=$(expr $count + 1)
done
